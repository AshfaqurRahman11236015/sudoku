package sudokusolver;

/**
 * @author Mahie Rahman 11236015
 * @author Coby Plain 11402854
 * 
 * The Solver is built to be loosely coupled with any interface. As such it deals only with int[][] as input and
 * output. It is up to a controlling class to make the conversion from UI to datatype. 
 * 
 * Ideally as the final solution would contain a number of classes similar to Solver for hints etc we would
 * actually build a managing Engine class that sits between these core classes and any GUI to add further 
 * abstraction. The GUI would then only deal with the single Engine and the Engine would distribute the requests
 * to the relevant component class.
 * 
 * We have not done this yet as we have only built a single feature so this would be over-engineering
 * 
 */
public class Solver {
    
    public static final int EMPTY = 0;
    
    private static final int START_ROW = 0;
    private static final int START_COLUMN = 0;
    
    private final int sudokuFactor, sudokuLength; //provided upon construction
    private int[][] board;
    
    public Solver(int sudokuFactor) {
        this.sudokuFactor = sudokuFactor;
        this.sudokuLength = sudokuFactor * sudokuFactor;
    }

    public int[][]solveEntireSudoku(int[][]board) {
        this.board = board;
        guess(START_ROW, START_COLUMN); // Recursive method, will solve the whole thing and store solution in this.board
        return this.board;
    }
    
    //Setting number based on the check method result
    private boolean guess(int row, int col) {
        int nextCol = (col + 1) % sudokuLength;
        int nextRow = (nextCol == EMPTY) ? row + 1 : row;
        try {
            if(board[row][col] != EMPTY) {
                return guess(nextRow, nextCol);
            }
        } catch(ArrayIndexOutOfBoundsException e) {
            return true;
        }
        
        for(int i = 1; i <= sudokuLength; i++) {
            if(check(i, row, col)) {
                board[row][col] = i;
                if(guess(nextRow, nextCol)) {
                    return true;
                }
            }
        }
        board[row][col] = EMPTY;
        return false;
    }
    
    //Checking whether the number is found in the row, column or factor*factor box
    private boolean check(int num, int row, int col) {
        int r = (row / sudokuFactor) * sudokuFactor;
        int c = (col / sudokuFactor) * sudokuFactor;
        
        for (int i = 0; i < sudokuLength; i++) {
            if(board[row][i] == num || 
               board[i][col] == num ||
               board[r + (i % sudokuFactor)][c + (i / sudokuFactor)] == num) {
                return false;
            }
        }
        return true;
    }
    
}
