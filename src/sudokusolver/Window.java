package sudokusolver;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * @author Mahie Rahman 11236015
 * @author Coby Plain 11402854
 */
public class Window extends JFrame {
    
    public Window() {
        setup();
        build();
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }
    
    private void setup() {
        setResizable(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
    private void build() {
        
        //we have added this to demonstrate the flexibilty of the design. A final solution would have this feature better integrated
        String baseFactorString = JOptionPane.showInputDialog(null, "Set base factor of Sudoku\nFor performance reasons we recommend a max of 4", 3);
        try {
            int baseFactorInt = Integer.parseInt(baseFactorString); // entire solution will scale based on this, including solver
            add(new SudokuGUI(baseFactorInt));
        }
        catch (Exception e) {
            //As we are controlling the demonstration we are not currenlty concerned with managing unexpected inputs
            //beyond a basic level. Finer grade exception handling will be relvant in future expansion
            JOptionPane.showMessageDialog(null, "Sorry, the factor must be a number. Ending program");
            System.exit(0);
        }
    }
}
