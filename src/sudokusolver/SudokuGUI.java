package sudokusolver;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * @author Mahie Rahman 11236015
 * @author Coby Plain 11402854
 * 
 * This is a simple, disposable GUI class. It internally manages the conversion between JTextField content
 * and int[][] as well as feeding this into the Solver. It has simplistic error handling. Detecting for 'empty'
 * fields as defined by the Solver. Care has been taken to keep Solver logic and UI logic separate so that in
 * theory any interface could be used on our Solver.
 * 
 */
public class SudokuGUI extends JPanel {

    private static final int INPUT_ROWS = 1;
    
    private final int sudokuFactor, sudokuLength; //provided upon construction
    private final Solver sudokuEngine;
    private final JTextField[][] sudokuMap; //used to map textfields to dimensions we understand
    
    private JButton btnSolve, btnClear;

    public SudokuGUI(int sudokuFactor) {
        
        //need to be assigned in constructor as are final
        this.sudokuFactor = sudokuFactor;
        this.sudokuLength = sudokuFactor * sudokuFactor;
        
        sudokuMap = new JTextField[sudokuLength][sudokuLength];
        sudokuEngine = new Solver(sudokuFactor);
        
        setupViews();
        build();
    }

    private void setupViews() {
        btnSolve = new JButton("Solve");
        btnClear = new JButton("Clear");
        
        btnSolve.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                runSolveOperation();
            }
        });
        
        btnClear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clearBoard();
            }
        });
    }
    
    private void build() {
        setLayout(new GridLayout(sudokuFactor + INPUT_ROWS, 1));
        for (int i = 0; i < sudokuFactor; i++) {
            add(createHorizontalSudokuBox(i)); //each large horizontal row, will create sub cells and input cells inside method
        }
        add(createInputBox()); //input row
    }
    
    private Box createHorizontalSudokuBox(int row) {
        Box box = Box.createHorizontalBox();
        for (int i = 0; i < sudokuFactor; i++) { 
            box.add(gridPanel(row, i)); //each sub cell, creates input cells inside method
        }
        return box;
    }
    
    private JPanel gridPanel(int boxRow, int boxNumber) {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(sudokuFactor, sudokuFactor));
        panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        for(int i = 0; i < sudokuLength; i++) {
            //create field
            JTextField field = new JTextField(2);
            field.setHorizontalAlignment(JTextField.CENTER);
            panel.add(field);
            
            //store reference to field as xy from top left corner so we can access it in future
            int actualRow = (boxRow * sudokuFactor) + (i / sudokuFactor); 
            int actualColumn = (boxNumber * sudokuFactor) + (i % sudokuFactor);
            sudokuMap[actualRow][actualColumn] = field;
        }
        return panel;
    }
    
    private Box createInputBox() {
        Box iBox = Box.createHorizontalBox();
        iBox.add(btnSolve);
        iBox.add(btnClear);
        return iBox;
    }
    
    private void runSolveOperation() {
        int[][]solvedBoard = sudokuEngine.solveEntireSudoku(getBoardArray());
        setBoardArray(solvedBoard);
    }
    
    private void clearBoard() {
        String[][]solvedBoard = new String[sudokuLength][sudokuLength]; //empty array to overwrite current display
        setBoardArray(solvedBoard);
    }
    
    private int[][] getBoardArray() {
    
        int[][] board = new int[sudokuLength][sudokuLength];
        
        for(int i = 0; i < sudokuLength; i++) {
            for (int j = 0; j < sudokuLength; j++) {
                board[i][j] = getValueAt(i, j);
            }
        }
        
        return board;
    }
    
    //int version
    private void setBoardArray(int[][] solution) {
        
        for(int i = 0; i < sudokuLength; i++) {
            for (int j = 0; j < sudokuLength; j++) {
                setFieldAt(i, j, solution[i][j]);
                checkOutputError(solution[i][j]);
            }
        }
        
    }
    
    //String version, used for field clearing
    private void setBoardArray(String[][] solution) {
        
        for(int i = 0; i < sudokuLength; i++) {
            for (int j = 0; j < sudokuLength; j++) {
                setFieldAt(i, j, solution[i][j]);
            }
        }
        
    }
    
    private int getValueAt(int x, int y) {
        
        String contents = sudokuMap[x][y].getText();
        
        if (contents == null || contents.equals("")) {
            return Solver.EMPTY;
        }
        else {
            return Integer.valueOf(contents);
        }
        
    }
    
    //int version
    private void setFieldAt(int x, int y, int value) {
        setFieldAt(x, y, Integer.toString(value));
    }
    
    //string version
    private void setFieldAt(int x, int y, String value) {
        sudokuMap[x][y].setText(value);
    }
    
    // display dialog if field is 'empty'
    private void checkOutputError(int output) {
        if (output == Solver.EMPTY) {
            JOptionPane.showMessageDialog(null, "Sorry there was an error solving your puzzle, cells we couldn't solve are marked as " + output);
        }
    }
    
}
